require 'test_helper'

class ListActivitiesControllerTest < ActionController::TestCase
  setup do
    @list_activity = list_activities(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:list_activities)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create list_activity" do
    assert_difference('ListActivity.count') do
      post :create, list_activity: { Grade: @list_activity.Grade, Gradeoutof: @list_activity.Gradeoutof, Name: @list_activity.Name, Percentage: @list_activity.Percentage, ShortName: @list_activity.ShortName }
    end

    assert_redirected_to list_activity_path(assigns(:list_activity))
  end

  test "should show list_activity" do
    get :show, id: @list_activity
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @list_activity
    assert_response :success
  end

  test "should update list_activity" do
    patch :update, id: @list_activity, list_activity: { Grade: @list_activity.Grade, Gradeoutof: @list_activity.Gradeoutof, Name: @list_activity.Name, Percentage: @list_activity.Percentage, ShortName: @list_activity.ShortName }
    assert_redirected_to list_activity_path(assigns(:list_activity))
  end

  test "should destroy list_activity" do
    assert_difference('ListActivity.count', -1) do
      delete :destroy, id: @list_activity
    end

    assert_redirected_to list_activities_path
  end
end

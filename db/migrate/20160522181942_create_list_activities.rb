class CreateListActivities < ActiveRecord::Migration
  def change
    create_table :list_activities do |t|
      t.text :Name
      t.text :ShortName
      t.float :Grade
      t.float :Gradeoutof
      t.float :Percentage

      t.timestamps null: false
    end
  end
end

class CreateActivities < ActiveRecord::Migration
  def change
    create_table :activities do |t|
      t.float :grade
      t.float :gradeoutof

      t.timestamps null: false
    end
  end
end

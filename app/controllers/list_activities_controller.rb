class ListActivitiesController < ApplicationController
  before_action :set_list_activity, only: [:show, :edit, :update, :destroy]

  # GET /list_activities
  # GET /list_activities.json
  def index
    @list_activities = ListActivity.all
  end

  # GET /list_activities/1
  # GET /list_activities/1.json
  def show
  end

  # GET /list_activities/new
  def new
    @list_activity = ListActivity.new
  end

  # GET /list_activities/1/edit
  def edit
  end

  # POST /list_activities
  # POST /list_activities.json
  def create
    @list_activity = ListActivity.new(list_activity_params)

    respond_to do |format|
      if @list_activity.save
        format.html { redirect_to @list_activity, notice: 'List activity was successfully created.' }
        format.json { render :show, status: :created, location: @list_activity }
      else
        format.html { render :new }
        format.json { render json: @list_activity.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /list_activities/1
  # PATCH/PUT /list_activities/1.json
  def update
    respond_to do |format|
      if @list_activity.update(list_activity_params)
        format.html { redirect_to @list_activity, notice: 'List activity was successfully updated.' }
        format.json { render :show, status: :ok, location: @list_activity }
      else
        format.html { render :edit }
        format.json { render json: @list_activity.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /list_activities/1
  # DELETE /list_activities/1.json
  def destroy
    @list_activity.destroy
    respond_to do |format|
      format.html { redirect_to list_activities_url, notice: 'List activity was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_list_activity
      @list_activity = ListActivity.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def list_activity_params
      params.require(:list_activity).permit(:Name, :ShortName, :Grade, :Gradeoutof, :Percentage)
    end
end

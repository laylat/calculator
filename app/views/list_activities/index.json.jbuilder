json.array!(@list_activities) do |list_activity|
  json.extract! list_activity, :id, :Name, :ShortName, :Grade, :Gradeoutof, :Percentage
  json.url list_activity_url(list_activity, format: :json)
end
